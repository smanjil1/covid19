
import tabula
import camelot
import textract

# fname = '20200121-sitrep-1-2019-ncov.pdf'
fname = '20200225-sitrep-36-covid-19.pdf'

# dfs = tabula.read_pdf(fname, pages='all', stream=True)
# #
# for i, row in enumerate(dfs):
#     row.to_csv(f'test{i}.csv', index=False)

# df = camelot.read_pdf(fname, flavor='stream', strip_text='\n',
#                               pages='2-3')
# for i, table in enumerate(df):
#     table.to_csv(f'table{i}.csv', index=False)

text = textract.process(fname, method='tesseract')
print(text)
