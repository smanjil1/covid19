
## Use Python3.7 environment (Ubuntu 18.04)

## Install dependencies
```
$ pip install -r requirements.txt

$ sudo apt install openjdk-8-jdk -y
```

## Run scraping script:
```
$ python who_pdf_scrape.py
```
