
import os
import camelot

from bs4 import BeautifulSoup
import urllib.request as urllib2


DOMAIN_URL = 'https://www.who.int'
BASE_URL = 'https://www.who.int/emergencies/diseases/novel-coronavirus-2019/situation-reports'
PDF_DIR = os.path.join(os.getcwd(), 'COVID19')
CSV_DIR = os.path.join(os.getcwd(), 'CSVS')


def get_pdfs():
    # get response for the base url
    req = urllib2.Request(BASE_URL)
    req = urllib2.urlopen(req)

    # request the url
    html = BeautifulSoup(req.read(), 'html.parser')

    # get all pdf links
    pdf_links = html.find_all('p')
    for elem in pdf_links:
        a_links = elem.find('a')

        if a_links:
            pdf_url = ''.join([DOMAIN_URL, a_links.attrs['href']])
        else:
            a_links = elem.find('strong')
            if a_links:
                a_links = a_links.find('a')
                pdf_url = ''.join([DOMAIN_URL, a_links.attrs['href']])

        # separate fname from link
        fname = pdf_url.split('situation-reports/')[-1].split('?')[0]

        # create download dir if not present
        if not os.path.exists(PDF_DIR):
            os.makedirs(PDF_DIR)

        # download only if the file is not present
        if fname not in os.listdir(PDF_DIR):
            # download
            os.system(f"wget -O {PDF_DIR}/{fname} {pdf_url}")


def save_csv(dir_name, df):
    if not os.path.exists(f'{CSV_DIR}/{dir_name}'):
        os.makedirs(f'{CSV_DIR}/{dir_name}')

        for i, table in enumerate(df):
            table.to_csv(f'{CSV_DIR}/{dir_name}/table{i}.csv', index=False)


def extract_tables():
    for fname in os.listdir(PDF_DIR):
        fpath = os.path.join(PDF_DIR, fname)

        df = camelot.read_pdf(fpath, flavor='stream', strip_text=' .\n',
                              pages='all')

        # extract pdf file initial number to create directory for csvs
        csv_sub_dir = fname.split('.')[0]

        # save df as individual csv
        save_csv(csv_sub_dir, df)


if __name__ == '__main__':
    get_pdfs()
    extract_tables()
